# [NeoFrag CMS](http://www.neofrag.fr/) :: Thème Dungeon

[Dungeon](https://github.com/NeoFragCMS/neofrag-theme-dungeon) est un thème orienté jeux vidéo, facilement personnalisable avec un choix de couleur illimité.
Créé par [Alessandro STIGLIANI](https://dribbble.com/nxalessandro) avec une mise en page moderne, il s'adapte pour tout type d'univers !

Thème compatible avec la version **NeoFrag Alpha 0.1.2** minimum.

## Installer le thème

Pour installer ce thème sur NeoFrag c'est très simple. Rendez-vous dans l'administration de votre site, rubrique **"Apparence"** > **"Thèmes"**

* Cliquez sur le bouton **Installer / Mettre à jour un thème**
* Sélectionnez l'archive du thème dungeon **.zip**
* Valider et attendez la fin du processus d'installation du thème

Une fois le processus terminé, cliquez sur la vignette du thème pour l'activer et le tour est joué !

## Bugs et remarques

Un problème ou une remarque à nous signaler concernant ce thème ? Venez nous en faire part sur le [site NeoFrag](http://www.neofrag.fr) ou sur GitHub.

## Créateur

Dungeon est un thème imaginé par [Alessandro STIGLIANI](https://dribbble.com/nxalessandro) et adapté par l'équipe NeoFrag.
Retrouvez le sur twitter et Dribble.

* https://twitter.com/nxalessandro
* https://dribbble.com/nxalessandro

L'équipe NeoFrag remercie **Alessandro** pour le partage de son design !

## Licence

Dungeon est un thème distribué gratuitement sous la licence [Creative Commons CC BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)